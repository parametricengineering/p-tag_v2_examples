![Parametric GmbH](https://www.parametric.ch/assets/images/logos/logo.png)

# Inhaltsverzeichnis

* [Basic Example](https://bitbucket.org/parametricengineering/p-tag_v2_examples/src/master/Basic/)
* [Dokumentation P-Tags](https://www.npmjs.com/package/p-tags_v2)




## Basic Example
Grundlegendes Beispiel für die Verwendung der P-Tags mit zwei Geräten.
![Parametric GmbH](/Assets/images/screenshotBasicExample.PNG)

